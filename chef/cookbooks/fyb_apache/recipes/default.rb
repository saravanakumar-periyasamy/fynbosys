#
# Cookbook Name:: fyb_apache
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package "apache2" do
	action :install	
end

template "/etc/apache2/sites-enabled/000-default.conf" do
	source "000-default.conf.erb"	
	variables(
  		:port => node['fyb_apache']['port'],
  		:server_name => node['fyb_apache']['server_name'],
  		:document_root => node['fyb_apache']['document_root']
  	)
  	notifies :restart, 'service[apache2]'
end

template "/etc/apache2/ports.conf" do
	source "ports.conf.erb"	
	variables(
  		:port => node['fyb_apache']['port']
  	)
  	notifies :restart, 'service[apache2]'
end

service "apache2" do
	action :start
end